# Day 12 Advent of Code 2019

Parametric motion is the name of the game in this problem. 

Story aside, there are 4 parametric bodies that start out with a velocity matrix of 0. 

# Part 1

Sampel input:
```text
<x=-1, y=0, z=2>
<x=2, y=-10, z=-7>
<x=4, y=-8, z=8>
<x=3, y=5, z=-1>
```

The point is to track movements. Gravity was applied to the position. From the problem "To apply gravity, consider every pair of moons. On each axis (`x`, `y`, and `z`), the velocity of each moon changes by **exactly +1 or -1** to pull the moons together." The problem should take an input for how many stpes to take with the problem input and return. For the problem output at `n-th` step is going to be position `pot=x+y+z` and velocity `kin=x+y+z` vectors for a final value of `sum = pot * kin`. Each sum for each moon from the problem are then added together for a final sum.

```text
After 0 steps:
pos=<x=-1, y=  0, z= 2>, vel=<x= 0, y= 0, z= 0>
pos=<x= 2, y=-10, z=-7>, vel=<x= 0, y= 0, z= 0>
pos=<x= 4, y= -8, z= 8>, vel=<x= 0, y= 0, z= 0>
pos=<x= 3, y=  5, z=-1>, vel=<x= 0, y= 0, z= 0>

After 1 step:
pos=<x= 2, y=-1, z= 1>, vel=<x= 3, y=-1, z=-1>
pos=<x= 3, y=-7, z=-4>, vel=<x= 1, y= 3, z= 3>
pos=<x= 1, y=-7, z= 5>, vel=<x=-3, y= 1, z=-3>
pos=<x= 2, y= 2, z= 0>, vel=<x=-1, y=-3, z= 1>

After 2 steps:
pos=<x= 5, y=-3, z=-1>, vel=<x= 3, y=-2, z=-2>
pos=<x= 1, y=-2, z= 2>, vel=<x=-2, y= 5, z= 6>
pos=<x= 1, y=-4, z=-1>, vel=<x= 0, y= 3, z=-6>
pos=<x= 1, y=-4, z= 2>, vel=<x=-1, y=-6, z= 2>

After 3 steps:
pos=<x= 5, y=-6, z=-1>, vel=<x= 0, y=-3, z= 0>
pos=<x= 0, y= 0, z= 6>, vel=<x=-1, y= 2, z= 4>
pos=<x= 2, y= 1, z=-5>, vel=<x= 1, y= 5, z=-4>
pos=<x= 1, y=-8, z= 2>, vel=<x= 0, y=-4, z= 0>

After 4 steps:
pos=<x= 2, y=-8, z= 0>, vel=<x=-3, y=-2, z= 1>
pos=<x= 2, y= 1, z= 7>, vel=<x= 2, y= 1, z= 1>
pos=<x= 2, y= 3, z=-6>, vel=<x= 0, y= 2, z=-1>
pos=<x= 2, y=-9, z= 1>, vel=<x= 1, y=-1, z=-1>

After 5 steps:
pos=<x=-1, y=-9, z= 2>, vel=<x=-3, y=-1, z= 2>
pos=<x= 4, y= 1, z= 5>, vel=<x= 2, y= 0, z=-2>
pos=<x= 2, y= 2, z=-4>, vel=<x= 0, y=-1, z= 2>
pos=<x= 3, y=-7, z=-1>, vel=<x= 1, y= 2, z=-2>

After 6 steps:
pos=<x=-1, y=-7, z= 3>, vel=<x= 0, y= 2, z= 1>
pos=<x= 3, y= 0, z= 0>, vel=<x=-1, y=-1, z=-5>
pos=<x= 3, y=-2, z= 1>, vel=<x= 1, y=-4, z= 5>
pos=<x= 3, y=-4, z=-2>, vel=<x= 0, y= 3, z=-1>

After 7 steps:
pos=<x= 2, y=-2, z= 1>, vel=<x= 3, y= 5, z=-2>
pos=<x= 1, y=-4, z=-4>, vel=<x=-2, y=-4, z=-4>
pos=<x= 3, y=-7, z= 5>, vel=<x= 0, y=-5, z= 4>
pos=<x= 2, y= 0, z= 0>, vel=<x=-1, y= 4, z= 2>

After 8 steps:
pos=<x= 5, y= 2, z=-2>, vel=<x= 3, y= 4, z=-3>
pos=<x= 2, y=-7, z=-5>, vel=<x= 1, y=-3, z=-1>
pos=<x= 0, y=-9, z= 6>, vel=<x=-3, y=-2, z= 1>
pos=<x= 1, y= 1, z= 3>, vel=<x=-1, y= 1, z= 3>

After 9 steps:
pos=<x= 5, y= 3, z=-4>, vel=<x= 0, y= 1, z=-2>
pos=<x= 2, y=-9, z=-3>, vel=<x= 0, y=-2, z= 2>
pos=<x= 0, y=-8, z= 4>, vel=<x= 0, y= 1, z=-2>
pos=<x= 1, y= 1, z= 5>, vel=<x= 0, y= 0, z= 2>

After 10 steps:
pos=<x= 2, y= 1, z=-3>, vel=<x=-3, y=-2, z= 1>
pos=<x= 1, y=-8, z= 0>, vel=<x=-1, y= 1, z= 3>
pos=<x= 3, y=-6, z= 1>, vel=<x= 3, y= 2, z=-3>
pos=<x= 2, y= 0, z= 4>, vel=<x= 1, y=-1, z=-1>
```

Solution run example: 
```
$ python d12.py -r 1 -p1 d12p1data.txt -i1 1000


Part 1:
5,4,4
-11,-11,-3
0,7,0
-13,2,10


After 1000 steps: 
9,55,20  -20,2,-14
-63,-15,-61  6,15,1
-12,19,33  8,0,3
47,-57,19  6,-17,10

Total Energy: 10845
```

# Part 2

Taking the information from above, the goal is to find when the moons will end up back into their original spots with a `0` velocity vector. So to test the plan is to check each x,y,z position that it's back at it's intial start with a 0 velocity vector but to find a way to not search all vectors.

After looking at a solution for [Walk Mankowski](https://github.com/waltman/advent-of-code-2019) for day 12, the trick they used were to look for the x, y, and z vectors match the initial values (all moons). Then by taking the lowest common multiple between each of the found vectors by steps, calculate the step where x,y,z are back at the origin for all moons. By doing this, run time is less than 1000th of the run time trying to brute force finding the vector.

Solution: 
```
$ python d12.py -r 2 -p1 d12p1data.txt

Part 2:
z 102356
x 186028
y 231614
Final step: 551272644867044
```

# Solution Entire Problem

```
$ python d12.py -r 3 -p1 d12p1data.txt -i1 1000

Part 1:
5,4,4
-11,-11,-3
0,7,0
-13,2,10


After 1000 steps: 
9,55,20  -20,2,-14
-63,-15,-61  6,15,1
-12,19,33  8,0,3
47,-57,19  6,-17,10

Total Energy: 10845

Part 2:
z 102356
x 186028
y 231614
Final step: 551272644867044
```
