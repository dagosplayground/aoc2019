#!/usr/bin/python
"""
Python 3 for AoC 2019 Day 12
https://adventofcode.com/2019/day/12
"""
import argparse
import json
import copy
import math

axises = ['x','y','z']

class Moons:

	def __init__(self,moonData):
		"""Moon data for the problem"""
		self.moons = list() # 0 will be moon, 1 velocity
		for moon in moonData:
			self.moons.append([moon,Moons.InitVelocity()])

		self.initMoons = copy.deepcopy(self.moons)
		self.cycles = {'x':None,'y':None,'z':None}
		self.found = 0 
		self.step = 0

	def InitVelocity():
		"""Generates blank velocity vector"""
		return {'x':0,'y':0,'z':0}

	def CompareMoons(self,moonA,moonB):
		"""Compares postions and adjusts velocities between moons
		moonA: int index of moonA
		moonB: int index of moonB
		MoonA will the one that has the velocities adjusted"""

		for axis in axises:
			if self.moons[moonA][0][axis] < self.moons[moonB][0][axis]:     # compare axis posistions  
				self.moons[moonA][1][axis] += 1							    # adjust velocity
			elif self.moons[moonA][0][axis] > self.moons[moonB][0][axis]:
				self.moons[moonA][1][axis] -= 1

	def ApplyVelocities(self):
		"""Position + Velocity to all moons"""
		for moonnum,moon in enumerate(self.moons):
			for axis in axises:
				self.moons[moonnum][0][axis] += self.moons[moonnum][1][axis]

	def StepMoons(self,steps):
		"""Provides step information"""
		for step in range(steps):
			for moonnum,moon in enumerate(self.moons):
				for i in range(len(self.moons)):
					if i == moonnum:
						pass
					else:
						self.CompareMoons(moonnum,i)
			self.ApplyVelocities()
			self.step += 1

	def CalculateEnergy(self):
		"""Sum of absolute pos * sum of absolute vel summed together"""
		totalSum = 0

		for moon in self.moons:
			sumPos = 0
			sumVel = 0
			for axis in axises:
				sumPos += abs(moon[0][axis])
				sumVel += abs(moon[1][axis])
			totalSum += sumPos * sumVel
		return totalSum 

	def MatchInit(self):
		"""Checks the current data to see if matches the initial."""
		for axis in axises: 
			if self.cycles[axis] is None:
				match = True
				for moonnum,moon in enumerate(self.moons):
					# Check moon position match first, then velocity match from initial.
					if self.moons[moonnum][0][axis] != self.initMoons[moonnum][0][axis] \
						or self.moons[moonnum][1][axis] != self.initMoons[moonnum][1][axis]:
						match = False
						break
				if match:
					self.cycles[axis] = self.step 
					print(f"{axis} {self.cycles[axis]}")
					self.found += 1

	def LowestCommonMultiple(a,b):
		"""Grabs the lowest common multiple of 2 numbers."""
		return (a*b) // math.gcd(a,b)

	def GetStep(self):
		"""Gets the step, solution to part 2"""
		residual = Moons.LowestCommonMultiple(self.cycles['x'],self.cycles['y'])
		residual = Moons.LowestCommonMultiple(residual,self.cycles['z'])
		return residual

	def __str__(self):
		"""Print as string"""
		retval = ""
		retval += "Inintial Moon Position, velocity\n"
		for moon in self.initMoons:
			retval += f"<{moon[0]['x']},{moon[0]['y']},{moon[0]['z']}>, {moon[1]['x']},{moon[1]['y']},{moon[1]['z']}>\n"
		retval += "Moon Position, velocity\n"
		for moon in self.moons:
			retval += f"<{moon[0]['x']},{moon[0]['y']},{moon[0]['z']}>, {moon[1]['x']},{moon[1]['y']},{moon[1]['z']}>\n"
		retval += f"cyles: {self.cycles}\n"
		retval += f"found: {self.found}\n"
		retval += f"step: {self.GetStep()}\n"
		return retval 


def ReadFile(fileName):
	"""Reads input for the problem."""
	with open(fileName,'r') as infile:
		data = infile.readlines()
		moons = list() 
		for line in data:
			tmpstring = line.strip('\n')
			tmpstring = tmpstring.strip('<')
			tmpstring = tmpstring.strip('>')
			cartcoords = tmpstring.split(',')
			vector = {}
			for component in cartcoords:
				axis,mag = component.split('=')
				vector[axis.strip(" ")] = int(mag) 
			moons.append(vector)
		# print(moons)
		return moons 

def part1(fileName,steps):
	"""Figuring out the basics"""
	moonPositions = ReadFile(fileName)
	moons = Moons(moonPositions)
	for moon,velocity in moons.moons: 
		print(f"{moon['x']},{moon['y']},{moon['z']}")
	print("\n")
	moons.StepMoons(int(steps))
	print(f"After {steps} steps: ")
	for moon,velocity in moons.moons: 
		print(f"{moon['x']},{moon['y']},{moon['z']}  {velocity['x']},{velocity['y']},{velocity['z']}")

	print(f"\nTotal Energy: {moons.CalculateEnergy()}")

def part2(fileName):
	"""How many steps to start from scratch"""
	moonPositions = ReadFile(fileName)
	moons = Moons(moonPositions)
	# for moon,velocity in moons.moons: 
	# 	print(f"{moon['x']},{moon['y']},{moon['z']}")

	moons.StepMoons(1)
	while moons.found < 3:
		moons.StepMoons(1)
		moons.MatchInit()

	# print(moons)
	# print(f"Sum of cycles: {sum(moons.cycles)} ")
	print(f"Final step: {moons.GetStep()}")

def main(parts,infile,steps):
	"""Main method to call both parts of the problem. """

	if parts == 3 or parts == 1:
		print("\nPart 1:")
		part1(infile,steps[0])

	if parts == 3 or parts == 2:
		print("\nPart 2:")
		part2(infile)

if __name__ == '__main__':
	# Accept command line arguments in a cleaner like fashion.

	parser = argparse.ArgumentParser(description='Advent of Code 2019 Day 12: Elf password pictograms')
	parser.add_argument('-p1','--d12p1File', help="Name of the Day12 part 1 input text file") 
	parser.add_argument('-i1','--input1', help="Steps input for part 1") 
	parser.add_argument('-r','--runpart', type=int, help="Set which part to run, 1 for part 1, 2 for part 2, 3 for part 1 and 2.")

	args = parser.parse_args()
	infile = args.d12p1File
	partInfo = [args.input1]
	main(args.runpart,infile,partInfo)
