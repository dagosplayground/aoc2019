#!/usr/bin/python
"""
Python 3 for AoC 2019 Day 10
https://adventofcode.com/2019/day/10
"""
import argparse
from collections import deque
from collections import defaultdict 
import math


def ReadFile(filename):
	"""Reads the file and returns an array of strings comma deliniated"""
	with open(filename,'r') as programfile:  
		asteroidmap = programfile.readlines()

		return asteroidmap

def Getasteroids(mapInput):
	"""Takes the string input and finds asteroids. 
	Asteroids are '#' and space is '.'.
	input: string array. Each line is a string. 
	returns: deque of asteroids with positions in x,y"""

	asteroids = deque()
	for ycoord,line in enumerate(mapInput):
		for xcoord,char in enumerate(line):
			if char == '#':
				asteroids.append((xcoord,ycoord))
	return asteroids

def AngleOffset(cartCoords):
	"""Finds the angle offset (in radians) based on the x,y cartesian coordinates
	cartCoords: touple or list of x,y coords
	returns: offset angle in radians"""
	x,y = cartCoords
    
	if x >= 0 and y <= 0:  
		return 0
	elif x >= 0 and y > 0: 
		return math.pi/2  
	elif x < 0 and y > 0:
		return math.pi 
	elif x < 0 and y < 0: 
		return (3 * math.pi)/2

	# if x >= 0 and y >= 0:  
	# 	return 0
	# elif x < 0 and y >= 0: 
	# 	return math.pi/2  
	# elif x < 0 and y < 0:
	# 	return math.pi 
	# elif x >= 0 and y < 0: 
	# 	return (3 * math.pi)/2

def GetAngles(asteroid,asteroidMap):
	"""Get the angles of all asteroids from the reference. 
	asteroid: Single asteroid object as defined from Getasteroids
	asteroidMap: Map of all asteroids
	returns: something with all angles. """

	origX, origY = asteroid
	angulars = deque()

	for pointX,pointY in asteroidMap:
		angCoord = (origX-pointX,origY-pointY) # Cartesian coordinates x,y
		distance = math.sqrt( angCoord[0]**2 + angCoord[1]**2 ) 
		angle = math.atan2(angCoord[1],angCoord[0]) - math.pi/2
		# angle = math.atan2(angCoord[1],angCoord[0])
		if angle < 0: 
			angle += 2 * math.pi
		angle = angle*180/math.pi
		# try: 
		# 	angle = AngleOffset(angCoord) + math.atan(math.fabs(angCoord[1])/math.fabs(angCoord[0]))
		# except ZeroDivisionError:
		# 	angle = AngleOffset(angCoord) 
		if angCoord != (0,0):
			angulars.append((angle,distance,(pointX,pointY)))

	return angulars

def FindClosest(angulars):
	"""Takes a point and finds the closest valid points.
	angulars: iterable object of angle,magnitude"""
	
	observable = {} 

	for angle,magnitude,angCoord in angulars:
		try:
			if magnitude < observable[angle][0]: 
				observable[angle] = (magnitude, angCoord)
		except KeyError:
			observable[angle] = (magnitude, angCoord)

	# Convert to list 
	retval = deque()
	for i in observable:
		retval.append(observable[i][1])
	return retval  

def ClosestLayer(angulars):
	"""Finds the first layer of angles and have them sorted.
	angulars: map from GetAngles
	returns: sorted list of points sorted by angle, only the coordinates returned in order. 
	"""
	observable = {} 

	for angle,magnitude,angCoord in angulars:
		try:
			if magnitude < observable[angle][0]: 
				observable[angle] = (magnitude, angCoord)
		except KeyError:
			observable[angle] = (magnitude, angCoord)

	# Convert to list n
	unsorted = [] 
	for i in observable.keys():
		unsorted.append((i,observable[i][1]))

	unsorted.sort()

	sortedList = []
	for angle, coord in unsorted:
		sortedList.append(coord)

	return sortedList 

def FindMostObservable(asteroidMap):
	"""Reports the asteroid(s) that can abserve the most other asteroids.
	asteroidMap: asteroid map deque
	returns: (point,count) tuple. Point is a tuple x,y.
	"""
	maxCount = 0 
	maxPoint = None
	for asteroid in asteroidMap:
		asteroidAngles = GetAngles(asteroid,asteroidMap)
		observableasteroid = FindClosest(asteroidAngles)
		lenObs = len(observableasteroid) 
		if lenObs >= maxCount:
			maxCount = lenObs
			maxPoint = asteroid

	return (maxPoint,maxCount)

def part2(filename,asteroidNumber,visualize=False):
	"""Part2: Blows up asteroids one at a top and closests. 
	Once at asteroid number, matches asteroidNumber, return 
	x,y coordinates."""
	data = ReadFile(filename)
	amap = Getasteroids(data)

	print(f"Total number of asteroids: {len(amap)}")

	observationPoint,asteroidCount = FindMostObservable(amap)

	print(f"Observation point: {observationPoint}")
	zapped = 0
	finalPoint = None

	while len(amap) > 4:
		angles = GetAngles(observationPoint,amap)
		sortedLayer = ClosestLayer(angles)
		tenvals = []
		for point in sortedLayer:
			amap.remove(point)
			zapped += 1 
			tenvals.append(point)
			if visualize:
				print(f"{zapped} : {point} : {len(amap)}")
			tmpPoint = point
			if zapped % 10 == 0:
				newMap = ''
				tmpAsteroids = []
				# tmpAsteroids += amap 
				tmpAsteroids += tenvals 
				if visualize:
					for lineNum,line in enumerate(data):
						for rowNum,point in enumerate(line):
							try:
								if (rowNum,lineNum) == observationPoint: 
									newMap += 'X'
								elif indexNum := tmpAsteroids.index((rowNum,lineNum)):
									newMap += str(indexNum)
							except ValueError:
								newMap += '.'
						newMap += '\n'	
					print(newMap)
				newMap = ''
				tenvals = []
			if zapped == asteroidNumber:
				# print(f"Final point: { tmpPoint }")
				return point
	return finalPoint

def part1(filename):
	"""Part 1"""
	data = ReadFile(filename)
	amap = Getasteroids(data)

	print(f"Total number of asteroids: {len(amap)}")

	observationPoint,asteroidCount = FindMostObservable(amap)

	print(f"Best is {observationPoint} with {asteroidCount} other asteroids detected:")
	for line in data:
		print(line.strip("\n"))
	return 0


def main(parts,fileNames,indata=0):
	"""Main method to call both parts of the problem. """

	if parts == 3 or parts == 1:
		print("\nPart 1:")
		p1val = part1(fileNames[0])
		# print("Part 1 solution:", str(p1val))
	if parts == 3 or parts == 2:
		print("\nPart 2:")
		finalpoint = part2(fileNames[0], indata)	
		print(f"The final point {finalpoint} with the value of {finalpoint[0]*100 + finalpoint[1]}")

if __name__ == '__main__':
	# Accept command line arguments in a cleaner like fashion. 

	parser = argparse.ArgumentParser(description='Advent of Code 2019 Day 10: Elf password pictograms')
	parser.add_argument('-p1','--d09p1File', help="Name of the Day10 part 1 input text file") 
	parser.add_argument('-i2','--infopart2', help="Width and length as tuple of layer comma separated.")
	parser.add_argument('-p2','--d09p2File', help="Name of the Day10 part 2 input text file") 
	parser.add_argument('-r','--runpart', type=int, help="Set which part to run, 1 for part 1, 2 for part 2, 3 for part 1 and 2.")

	args = parser.parse_args()
	infiles = [args.d09p1File, args.d09p1File]
	# p1data = args.infopart1
	indata = int(args.infopart2) if args.infopart2 != None else 0 
	main(args.runpart,infiles,indata)