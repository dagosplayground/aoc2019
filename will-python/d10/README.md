# AoC Day 09 2019

Link for the problem: https://adventofcode.com/2019/day/10

This problem pulls a map where (`.`) is space and (`#`) are asteroids. The goal is
to find which asteroids get the greatest visibility of other asteroids for a 
'base station'. The grids given, the origin is a the top left corner. 

## Part 1

Find the best location for a base station. Example: 

```
.#..#                   .....           .7..7
.....                   .....           .....
#####  Best asteroid =  .....  scores = 67775
....#                   .....           ....7
...##                   ...#.           ...87
```

The goal is to read the maps and find the best postions as defined in the problem set. 

Note, the Asteroid are at the center of the `#`. So if there is an angle that is exactly 
the same as an asteroid behind it, that does not count. 

Solution: 

The solution took a bit to think about, but becomes QUITE easy when broken down into steps. 

1. Convert the `#` into coordinates with 0,0 being the top left or first line into a list. 
1. A function for each asteroid to create a deque of all other relative asteroids by angle, distance, and orignal point. Also note, use to the use of the tangent function an offset calculate the offset from 0 to 2 pi radians was made. If the tangent functions runs into a divide by zero value, only give the offset angle. 
1. A function take the previous deque and make a dictionary of all angles and magnitudes. If the magnitude is smaller, replace the previous one so only the closest asteroid of that angle is recorded. Take dictionary and convert into a deque to be returned. 
1. Finally post the data for the last asteroid to have the greatest number of other asteroids observable.

Answer: 
```
$ python d10.py -r 1 -p1 d10p1data.txt 

Part 1:
Total number of asteroids: 360
Best is (17, 22) with 288 other asteroids detected:
#...##.####.#.......#.##..##.#.
#.##.#..#..#...##..##.##.#.....
#..#####.#......#..#....#.###.#
...#.#.#...#..#.....#..#..#.#..
.#.....##..#...#..#.#...##.....
##.....#..........##..#......##
.##..##.#.#....##..##.......#..
#.##.##....###..#...##...##....
##.#.#............##..#...##..#
###..##.###.....#.##...####....
...##..#...##...##..#.#..#...#.
..#.#.##.#.#.#####.#....####.#.
#......###.##....#...#...#...##
.....#...#.#.#.#....#...#......
#..#.#.#..#....#..#...#..#..##.
#.....#..##.....#...###..#..#.#
.....####.#..#...##..#..#..#..#
..#.....#.#........#.#.##..####
.#.....##..#.##.....#...###....
###.###....#..#..#.....#####...
#..##.##..##.#.#....#.#......#.
.#....#.##..#.#.#.......##.....
##.##...#...#....###.#....#....
.....#.######.#.#..#..#.#.....#
.#..#.##.#....#.##..#.#...##..#
.##.###..#..#..#.###...#####.#.
#...#...........#.....#.......#
#....##.#.#..##...#..####...#..
#.####......#####.....#.##..#..
.#...#....#...##..##.#.#......#
#..###.....##.#.......#.##...##
```

## Part 2

So in classic AoC fashion, part 2 make one question their previous execution of
the code. The big piece that needs to change is the start angle. Right now, the 
start angle is to the right being 0 or 2pi. The shift now needs to be 90 deg or
pi/2 shift counter-clockwise. The easiest way will be modifying the angle 
offset code and changing counter-clockwise angles to clockwise angle 
measurements. This change should not impact part 1. Then angle needs to be tested
and verified. 

Example with visualization: 
```
$ python d10.py -r 2 -p1 sample3.txt -i2 11

Part 2:
Total number of asteroids: 40
Observation point: (1, 2)
1 : (1, 1) : 39
2 : (2, 0) : 38
3 : (2, 1) : 37
4 : (3, 1) : 36
5 : (6, 0) : 35
6 : (8, 0) : 34
7 : (8, 1) : 33
8 : (6, 2) : 32
9 : (9, 3) : 31
10 : (7, 3) : 30
..1...4.5..
.23....6..
.X....7....
.......9.8.
...........
...........
...........
...........
...........
..........

11 : (5, 3) : 29
The final point (5, 3) with the value of 503
``` 

Problem set: What is the point of the 200th asteroid with the final answer `X*100+Y`

Final solution: 
```
$ python d10.py -r 2 -p1 d10p1data.txt -i2 200

Part 2:
Total number of asteroids: 360
Observation point: (17, 22)
The final point (6, 16) with the value of 616
```