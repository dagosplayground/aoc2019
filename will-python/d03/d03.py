#!/usr/bin/python
"""
Python 3 for AoC 2019 Day 03
https://adventofcode.com/2019/day/3

"""


import argparse, math


class Director(): 

	def __init__(self,inputFile):
		"""
		Director is a class to ingest all the movements from a text file
		and to handle moments in that text file. 
		"""
		self.wiresMovements = []
		self.wireSets = []
		self.wirePoints = []
		with open(inputFile,'r') as inFile:
			lines = inFile.readlines() 
			for line in lines:
				self.wiresMovements.append(line.split(','))
				# print(x.strip('\n' for x in line.split(",")))
				# self.movements.append(x.strip('\n') for x in line.split(',') )

	def transverse(self):
		"""From the directions in movements, create the sets of points for all wires"""
		for wireMoves in self.wiresMovements:
			point = 0j
			points = set()
			listpoints = []
			for movement in wireMoves:
				# print(movement)
				heading,magnitude = Director.parseMovement(movement)			
				for step in range(magnitude):
					point += heading
					points.add(point)
					listpoints.append(point)
					# print(str(point))
			self.wireSets.append(points)
			self.wirePoints.append(listpoints)


	def parseMovement(themove):
		"""Takes each direction and magnitude, returns direction tuple"""
		if themove == '' or themove == '\n':
			return 'Z',0 
		else:
			heading = themove[0]
			magnitude = int(themove[1:])				
			direction = 0
			if heading == "U":
				direction += 1j
			elif heading == "D":
				direction -= 1j
			elif heading == "R":
				direction += 1
			elif heading == "L":
				direction -= 1
			return direction,magnitude

	def getIntersections(self):
		"""Returns intersections from transversed sets"""
		if len(self.wireSets) > 1:
			return self.wireSets[0].intersection(self.wireSets[1])
		else: 
			return None 

	def getClosestDistances(self):
		"""Calculates the distance from origin.""" 
		intersections = self.getIntersections()
		storedPoint = None
		storedDistance = None 
		for cross in intersections:
			distance = int(cross.real) + int(math.fabs(cross.imag))
			# print(str(cross) + " : " + str(distance))
			if storedDistance == None or distance <= storedDistance:
				storedPoint = cross
				storedDistance = distance

		return storedPoint, storedDistance

	def getSteps(self,crossing,wire):
		"""Retuns how many steps to find the point in a set"""
		return self.wirePoints[wire].index(crossing) + 1

	def getShortestPathCross(self):
		"""Find the shortest combined path of intersections."""
		intersections = self.getIntersections()
		storedPoint = None
		storedPathSteps = None # Combined steps of both wires 
		for cross in intersections:
			steps1 = self.getSteps(cross,0) 
			steps2 = self.getSteps(cross,1) 
			totalsteps = steps1+steps2
			# print(str(cross),str(totalsteps),str(steps1),str(steps2))

			if storedPathSteps == None or totalsteps < storedPathSteps:
				storedPoint = cross
				storedPathSteps = totalsteps

		return storedPoint, storedPathSteps


def part1(filename):
	"""Finds the closet point of intersections to the origin."""
	dir = Director(filename)
	dir.transverse()
	closestPoint, closestDistance = dir.getClosestDistances()
	print("Closest Distance:", str(closestPoint), str(closestDistance))

def part2(filename):
	"""Finds the interesction with least number of steps to the interection of both wires."""
	dir = Director(filename)
	dir.transverse()
	# print("Raw points wire 0")
	# position = 0
	# print(str(dir.wirePoints[0][15]))
	# for point in dir.wireSets[0]:
	# 	print(str(point),str(dir.wirePoints[0][position]))
	# 	position +=1;
	closestPoint, closestDistance = dir.getShortestPathCross()
	print("Least steps:", str(closestPoint), str(closestDistance))


def main(parts,fileNames):
	"""Main method to call both parts of the problem. """

	if parts == 3 or parts == 1:
		part1(fileNames[0])
		# p1val = part1(fileNames[0])
		# print("Part 1 solution:", str(p1val))
		pass
	if parts == 3 or parts == 2:
		part2(fileNames[0])
		# p2val = part2(fileNames[1])	
		# print("Part 2 solution:", str(p2val))
		pass

if __name__ == '__main__':
	# Accept command line arguments in a cleaner like fashion. 

	parser = argparse.ArgumentParser(description='Advent of Code 2019 Day 03: Tron guidance')
	parser.add_argument('-p1','--d01p1File', help="Name of the Day 03 part 1 input text file") 
	parser.add_argument('-p2','--d01p2File', help="Name of the Day 03 part 2 input text file") 
	parser.add_argument('-r','--runpart', type=int, help="Set which part to run, 1 for part 1, 2 for part 2, 3 for part 1 and 2.")

	args = parser.parse_args()
	infiles = [args.d01p1File, args.d01p1File]
	main(args.runpart,infiles)
