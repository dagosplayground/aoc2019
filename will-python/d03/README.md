# Day 03 AoC 2019

So this problem will go back to a similar problem in a previous year. 

Essentially it's a manhattan grid problem. We'll use the notation of 
real and imaginary to handle the coordinates with the origin based 
around the center. 

## Part 1

Lets get some basics down first. We need to rip apart a CSV to give 
the coordinates movement of up, down, left, right, and by how many. 

Uo: + j
Down: - j
Right: + 1 
Left: - 1


Then... it's dump all the values into sets and using the intersection 
method of sets, find the set with the shortest distance to the 
origin. I'll admit, if you don't read today's directions... you will
really get your wires crossed. *bows*

## Part 2 

Well... this step was annoying. The idea is to take the same concept
as before BUT find the wires with the shortest distance to an 
intersection. Why was this one hard? Well... python sets do not 
maintain order. MEANING a little massaging and creating a list in 
addition to a set was the laziest answer possible. RIP memory 
efficiency. 


## General 

As before, use is similar to before and a help is there for your 
convienence to play along. Example output: 

```bash
./d03.py -r 3 -p1 d03p1Data.txt 
Closest Distance: (1968-1279j) 3247
Least steps: (4537-1239j) 48054
```