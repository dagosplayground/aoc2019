#!/usr/bin/python
"""
Python 3 for AoC 2019 Day 02
https://adventofcode.com/2019/day/2


"""

import argparse

def computer(intArrayInput):
	"""Meat and potatoes of this challenge for parts 1 and 2. 
	Takes an integer array of 'operations and parameters' as defined in the 
	problem. Outputs the result in [0], or their lame excuse of an 
	accumlator. Regardless, WORST PROCESSOR EVER!!! Give me a case of beer
	and I'll write one better. I know, I did it as an intern.
	Cleaner description -------
	input: integer array
	output: only integer you care about.
	"""
	stack = intArrayInput
	pointer = 0
	# print("****",str(stack),"****")
	while(stack[pointer] != 99):
		# print("-----",str(stack[pointer+1:pointer+4]))
		if stack[pointer] == 1:
			reg1, reg2, dest = stack[pointer+1:pointer+4]
			# print(stack[reg1],stack[reg2])
			stack[dest] = stack[reg1] + stack[reg2]
			pointer += 4
			# print(stack)
		elif stack[pointer] == 2: 
			reg1, reg2, dest = stack[pointer+1:pointer+4]
			# print(stack[reg1],stack[reg2])
			stack[dest] = stack[reg1] * stack[reg2]
			pointer += 4
			# print(stack)
	return stack[0]

def part1(p1FileIn):
	"""Part1"""
	with open(p1FileIn,'r') as p1f:
		p1data = p1f.readlines()
		opcodes = [int(place) for place in p1data[0].split(',')]
		print("\nPart 1 Answer:",str(computer(opcodes)))
	
def part2(targetValue,fileName):
	"""Part 2: Find what makes the target value"""
	with open(fileName,'r') as p2f:
		p2data = p2f.readlines()
		init_opcodes = [int(place) for place in p2data[0].split(',')]


		for noun in range(100):
			for verb in range(100):
				stack = init_opcodes.copy(); 
				stack[1] = int(noun)
				stack[2] = int(verb)
				# print("N,V:",str(noun),str(verb))
				if targetValue == computer(stack):
					# print("Part 2 Answer:", str(100 * noun + verb))
					return 100 * noun + verb
	print("Balls") # << Should never see this. 

def main(parts,fileNames,targetValue=0):
	"""Main method to call both parts of the problem. """

	if parts == 3 or parts == 1:
		part1(fileNames[0])
	if parts == 3 or parts == 2 and targetValue != 1:
		p2ans = part2(targetValue,fileNames[0]) # Too lazy to fix the parameter FileName. 
		print("Part 2 Answer:", str(p2ans))

if __name__ == '__main__':
	# Accept command line arguments in a cleaner like fashion. 

	parser = argparse.ArgumentParser(description='Advent of Code 2019 Day 02: Invalid Instruction')
	parser.add_argument('-p1','--d02p1File', help="Name of the Day 02 part 1 input text file") 
	parser.add_argument('-p2','--d02p2File', help="Name of the Day 02 part 2 input text file") 
	parser.add_argument('-t','--d02p2TargetValue', help="Target value to find in the program")
	parser.add_argument('-r','--runpart', type=int, help="Set which part to run, 1 for part 1, 2 for part 2, 3 for part 1 and 2.")

	args = parser.parse_args()
	infiles = [args.d02p1File, args.d02p2File] # Lazy again, should only need the first name. 
	if args.d02p2TargetValue == None:
		main(args.runpart,infiles)
	else: 
		main(args.runpart,infiles,int(args.d02p2TargetValue))

