# AoC Day 09 2019

Link for the problem: https://adventofcode.com/2019/day/9

Relative Base mode. 

Essentially... there's a new parameter to the int computer. If the int computer
from day 7 was done VERY well, then today should be VERY simple with the 
support of the relative base mode. 

So... what *is* relative base mode? Relative base mode simply starts at 0 at
the start. AFTER that, an input of 9 will add whatever input to the relative 
base.

## Part 1 

Basically payting the pied piper for all the sins of the past and having to 
refactor for this parameter mode 2 across the entire computer class. Seeing 
a theme coming in the future, I made the intcomputer it's own class. Also 
new is switching from an array for opcodes to a new defaultdict (refactoring 
consequence). 

Despite a pesky bug on the input class (don't ask). Simple. 

Example for running sample code. 

```bash
$ python d09.py -r 1 -p1 sample1.txt 
Output at 4: 109
Output at 4: 1
Output at 4: 204
Output at 4: -1
Output at 4: 1001
Output at 4: 100
Output at 4: 1
Output at 4: 100
Output at 4: 1008
Output at 4: 100
Output at 4: 16
Output at 4: 101
Output at 4: 1006
Output at 4: 101
Output at 4: 0
Output at 4: 99
HALT!
$ python d09.py -r 1 -p1 sample2.txt 
Output at 6: 1219070632396864
HALT!
$ python d09.py -r 1 -p1 sample3.txt 
Output at 2: 1125899906842624
HALT!
```

With the data set included, the problem stated to include the input of `1` when prompted. 
```bash
$ python d09.py -r 1 -p1 d09p1data.txt 
Set value at pointer 26: 1
Output at 903: 2870072642
HALT!
```

# Part 2

Literally nothing changes, run time is noticiably a few miliseconds longer.
Only difference for Part 2 is the input required for the problem set is `2`.

```bash $ python d09.py -r 1 -p1 d09p1data.txt  Set value at pointer 26: 2
Output at 921: 58534 HALT! ```