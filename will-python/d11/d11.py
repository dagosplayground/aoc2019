#!/usr/bin/python
"""
Python 3 for AoC 2019 Day 11
https://adventofcode.com/2019/day/11
"""
import argparse
from collections import deque
from collections import defaultdict 
from intcomputer import ReadFile, computer

def part1(filein):
	data = ReadFile(filein)

class PaintBot(computer):
	directions = {
		'^': -1,     
		'>': 1j,
		'V': 1, 
		'<': -1j
	}
	# Original values... that were weird. 
	# directions = {
	# 	'^': 1,     
	# 	'>': 1j,
	# 	'V': -1, 
	# 	'<': -1j
	# }
	def __init__(self,opcode):
		"""Factory class for the paint bots."""
		computer.__init__(self,opcode)
		self.heading = '^'
		self.points = defaultdict() # Map of points
		self.paintCounter = 0
		self.currentPoint = 0j
		self.outputCounter = 0    # outputs are always in pairs. 

		# Over override since there are overridden methods in PaintBot 
		self.ops = {
			1: self.add,
			2: self.multiply,
			3: self.program_input,
			4: self.program_output,
			5: self.jit, 
			6: self.jiz,
			7: self.IfLessThan,
			8: self.IfEqual,
			9: self.RelativeBaseAdjust,
		}

		self.pointMax = 0j
		self.pointMin = 0j
		self.alreadyPainted = 0

	def CheckPoint(self,point):
		"""Checkpoint, takes point information and reads any potential paint"""
		try:
			return self.points[point]
		except KeyError:
			return None

	def PaintPoint(self,color):
		"""Paints or writes a point"""
		check = self.CheckPoint(self.currentPoint)

		self.points[self.currentPoint] = int(color)
		# paint = 'white' if color == 1 else 'black'
		# print(f"Painted {self.currentPoint} {paint}")

	def Movement(self,movementInput):
		"""Takes output from the program for movement, 0=left 1=right"""

		if self.heading == '^':     # up
			self.heading = '>' if movementInput == 1 else '<'
		elif self.heading == 'V': 	# down 
			self.heading = '>' if movementInput != 1 else '<'
		elif self.heading == '>':  	# right   
			self.heading = 'V' if movementInput == 1 else '^'
		elif self.heading == '<':  	# left   
			self.heading = 'V' if movementInput != 1 else '^'

		self.currentPoint += self.directions[self.heading]

		# Tracking valuees I was curious in checking. 
		if self.currentPoint.real > self.pointMax.real:
			self.pointMax = int(self.currentPoint.real) + int(self.pointMax.imag)*1j
		if self.currentPoint.imag > self.pointMax.imag:
			self.pointMax = int(self.currentPoint.imag)*1j + int(self.pointMax.real)
		if self.currentPoint.real < self.pointMin.real:
			self.pointMin = int(self.currentPoint.real) + int(self.pointMin.imag)*1j
		if self.currentPoint.imag < self.pointMin.imag:
			self.pointMin = int(self.currentPoint.imag)*1j + int(self.pointMin.real)


	def program_input(self,modes):
		"""Override method. """
		# modified for computer inputs 
		params = computer.getParams(modes,1)

		arg1 = self._ParamToInput(params[0],1)
		color = self.CheckPoint(self.currentPoint)
		if color == None:
			color = int(0); 
			self.paintCounter += 1
		else:
			self.alreadyPainted += 1
		self.stack[arg1] = int(color)

		self.pointer += 2
		self.outputCounter = 0

	def program_output(self,modes):
		"""Computer print out function for the paint bot. 
		First output is to paint, second it to turn."""
		params = computer.getParams(modes,1)

		arg1 = self._ParamToInput(params[0],1)
		outval = self.stack[arg1]

		if self.outputCounter == 0: 		# Paint first
			self.outputCounter += 1
			self.PaintPoint(int(outval))
		elif self.outputCounter == 1:		# Turn and move second
			self.outputCounter += 1;
			self.Movement(int(outval))
		else:
			print(f"WARNING! OUTPUT OF SYNC!!!")
			return -1
		self.pointer += 2


def part1(fileName):
	data = ReadFile(fileName)
	bot = PaintBot(data)
	bot.run()
	print(f"Min: {bot.pointMin} Max: {bot.pointMax}")
	print(f"Already painted: {bot.alreadyPainted}")
	return bot.paintCounter

def part2(fileName):
	data = ReadFile(fileName)
	bot = PaintBot(data)
	bot.points[0j] = int(1) # Sets first panel/squre to white. 
	bot.run()
	print(f"Min: {bot.pointMin} Max: {bot.pointMax}")
	print(f"Already painted: {bot.alreadyPainted}")
	# Now to make the print out of the output
	for x in range(int(bot.pointMax.real)-int(bot.pointMin.real)+1): # points top to bottom.
		tmpline = ''
		for y in range(int(bot.pointMax.imag)-int(bot.pointMin.imag)+1): # points left to right.
			try:
				point = x + int(bot.pointMin.real) + y*1j + int(bot.pointMin.imag)*1j
				color = int(bot.points[point]) 
				tmpline += '#' if color == 1 else ' '
			except KeyError:
				tmpline += ' '
		print(tmpline)
def main(parts,fileNames,indata=0):
	"""Main method to call both parts of the problem. """

	if parts == 3 or parts == 1:
		print("\nPart 1:")
		p1val = part1(fileNames[0])
		print("Part 1 solution:", str(p1val))
	if parts == 3 or parts == 2:
		print("\nPart 2:")
		part2(fileNames[0])
		pass

if __name__ == '__main__':
	# Accept command line arguments in a cleaner like fashion. 

	parser = argparse.ArgumentParser(description='Advent of Code 2019 Day 11: Elf password pictograms')
	parser.add_argument('-p1','--d11p1File', help="Name of the Day11 part 1 input text file") 
	parser.add_argument('-i2','--infopart2', help="Width and length as tuple of layer comma separated.")
	parser.add_argument('-p2','--d11p2File', help="Name of the Day11 part 2 input text file") 
	parser.add_argument('-r','--runpart', type=int, help="Set which part to run, 1 for part 1, 2 for part 2, 3 for part 1 and 2.")

	args = parser.parse_args()
	infiles = [args.d11p1File, args.d11p1File]
	# p1data = args.infopart1
	indata = int(args.infopart2) if args.infopart2 != None else 0 
	main(args.runpart,infiles,indata)