"""

https://adventofcode.com/2019/day/11

Int computer class. 

"""

from collections import defaultdict 

def ReadFile(filename):
	"""Reads the file and returns an array of strings comma deliniated"""
	with open(filename,'r') as programfile:  
		programdata = programfile.readlines()
		opcodes = defaultdict(int,{position:int(place) for position,place in enumerate(programdata[0].split(','))}) # computer instructions from text file.     
		return opcodes

from collections import defaultdict

class computer:

	def __init__(self,intArrayInput):
		"""Meat and potatoes of this challenge for parts 1 and 2. 
		Takes an integer array of 'operations and parameters' as defined in the 
		problem. Outputs the result in [0], or their lame excuse of an 
		accumlator. Regardless, WORST PROCESSOR EVER!!! Give me a case of beer
		and I'll write one better. I know, I did it as an intern.
		Cleaner description -------
		input: integer array 
		output: only integer you care about.
		"""
		self.stack = intArrayInput
		self.pointer = 0
		# self.pointer = savedPointer
		self.relativeBase = 0 
		self.ops = {
			1: self.add,
			2: self.multiply,
			3: self.program_input,
			4: self.program_output,
			5: self.jit,
			6: self.jiz,
			7: self.IfLessThan,
			8: self.IfEqual,
			9: self.RelativeBaseAdjust,
		}

	def run(self):
		"""Run through the program and push opcodes to functions"""
		try:
			while self.stack[self.pointer] != 99: 
				# print(f"Pointer: {self.printointer} ")
				instruction = self.stack[self.pointer] % 100 
				if len(str(self.stack[self.pointer])) > 2:
					modes = str(self.stack[self.pointer])[:-2]
				else: 
					modes = "0"
				tmpPoint = self.pointer
				self.ops[instruction](modes)
			else:
				print("HALT!")
				return "HALT"

		except:
			with open("error.txt","w") as errtxt: 
				print(f"fail on pointer {self.pointer} {self.stack[self.pointer]} ")
				errtxt.write(str(self.stack))
				raise 

	def getParams(modes,length):
		"""Get the modes from a the inputs"""
		retmodes = [] 
		for index in modes[::-1]:
			retmodes.append(int(index))

		for i in range(length - len(retmodes)):
			retmodes.append(0)

		return retmodes

	def add(self, modes):
		"""Addition function""" 
		params = computer.getParams(modes,3)

		arg1 = self._ParamToInput(params[0],1)
		arg2 = self._ParamToInput(params[1],2)
		arg3 = self._ParamToInput(params[2],3)

		self.stack[arg3] = self.stack[arg1] + self.stack[arg2]
		self.pointer += 4         

	def multiply(self, modes):
		"""Multiplication function""" 
		params = computer.getParams(modes,3)

		arg1 = self._ParamToInput(params[0],1)
		arg2 = self._ParamToInput(params[1],2)
		arg3 = self._ParamToInput(params[2],3)

		self.stack[arg3] = self.stack[arg1] * self.stack[arg2]
		self.pointer += 4         

	def program_input(self,modes):
		"""Takes input from a system when the insturction is given."""
		# modified for computer inputs 
		params = computer.getParams(modes,1)

		arg1 = self._ParamToInput(params[0],1)

		self.stack[arg1] = int(input(f"Set value at pointer {self.pointer+1}: "))
		self.pointer += 2

	def program_output(self,modes):
		"""Computer print out function"""
		params = computer.getParams(modes,1)

		arg1 = self._ParamToInput(params[0],1)

		outval = self.stack[arg1]
		# print(f"Program output { outval }")
		self.pointer += 2
		print(f"Output at {self.pointer}: {outval}")


	def jit(self,modes):
		"""Jump if True
		If the first argument is not zero, set pointer to arg2"""
		params = computer.getParams(modes,2)
		arg1 = self._ParamToInput(params[0],1)
		arg2 = self._ParamToInput(params[1],2)

		if self.stack[arg1] != 0:
			# self._PrgmCheckResize(self.stack[self.pointer+2])
			self.pointer = self.stack[arg2]
		else:
			self.pointer += 3


	def jiz(self,modes):
		"""Jump if Zero
		If the first argument is zero, set pointer to arg2"""
		params = computer.getParams(modes,2)
		arg1 = self._ParamToInput(params[0],1)
		arg2 = self._ParamToInput(params[1],2)

		if self.stack[arg1] == 0:
			# self._PrgmCheckResize(self.stack[self.pointer+2])
			self.pointer = self.stack[arg2]
		else:
			self.pointer += 3

	def IfLessThan(self,modes):
		"""Evaluate arg1 < arg2
		If true/false store 1/0 in the address of arg3"""
		params = computer.getParams(modes,3)

		arg1 = self._ParamToInput(params[0],1)
		arg2 = self._ParamToInput(params[1],2)
		arg3 = self._ParamToInput(params[2],3)

		self.stack[arg3] = 1 if self.stack[arg1] < self.stack[arg2] else 0

		self.pointer +=4

	def IfEqual(self,modes):
		"""Evaluate arg1 == arg2
		If true/false store 1/0 in the address of arg3"""
		params = computer.getParams(modes,3)

		arg1 = self._ParamToInput(params[0],1)
		arg2 = self._ParamToInput(params[1],2)
		arg3 = self._ParamToInput(params[2],3)

		self.stack[arg3] = 1 if self.stack[arg1] == self.stack[arg2] else 0

		self.pointer +=4

	def RelativeBaseAdjust(self,modes):
		"""Adjust Relattive base"""
		params = computer.getParams(modes,1)
		arg1 = self._ParamToInput(params[0],1)
		self.relativeBase += self.stack[arg1]

		self.pointer += 2

	def _ParamToInput(self,param,pointerOffset):
		"""Maps param value to defined variable"""
		retval = 0
		if param == 2:
			retval = self.relativeBase+self.stack[self.pointer+pointerOffset]
		elif param == 1:
			retval = self.pointer+pointerOffset
		else:
			retval = self.stack[self.pointer+pointerOffset]
		return retval

	def _PrgmCheckResize(self,pointerReference):
		"""Void function to ensure program stack is large enough and resize/copy contents to new stack."""
		try:
			self.stack[pointerReference]
		except IndexError as e:
			self.stack[pointerReference] = 0

	def _GetSlice(self, start, end):
		"""Gets a slice of the stack. Used for debugging purposes."""
		myslice = []
		for i in range(end-start+1):
			myslice.append(self.stack[i])
		return myslice
