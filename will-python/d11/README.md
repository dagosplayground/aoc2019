# Advent of Code Day 11 

https://adventofcode.com/2019/day/11

Day 11 seems to be grabbing everything done in previous problems. So hopefully
the code from previous projecs all roled into one.

Global knowns: Robots take int computer inputs. There is a camera that reads 
the square they are on. Ouput can only be a `1` for white or `0` for black. 
Camera reads `1` for white and `0` for black. Output for direction is `0` 
for left and `1` for right. 

## Part 1 

Something was written about painting robots? Essentially what is known is that 
a painting robot must be built from the int computer. 

What is knonwn: 
* Int computer is used, there is a program from input. 
* For this part, everything is black on the tile.
* The robot starts in the pointing up configuration.
* Must count all squares painted by the robot ( If already painted, they count only as 1)
* Program memory must be kept intact for the instance of the robot.
* Output come 2 at a time. Color, then direction to move. 

Work in Progress: Values do NOT look right. To use: 
```bash
$ python d11.py -1 -p1 d11p1data.txt
```

So, taking the computer from the Day 09, the flow of program is this.
* Bot reads input, input read sets outputcounter to 0. 
* First output paints the square 
* Second rotates the bot and moves forward 1 square.

```bash 
python d11.py -r 1 -p1 d11p1data.txt
Part 1:
HALT!
Min: (-19-47j) Max: (33+31j)
Already painted: 7351
Part 1 solution: 2160

```

## Part 2

2 things that are simple for part 2. 
1. Make the first square white.
1. Print out the value of the painted values. 

Had to adjust some formatting issue with the printing output. Ugh.. 

```bash
python d11.py -r 2 -p1 d11p1data.txt
Part 2:
HALT!
Min: 0j Max: (5+42j)
Already painted: 1
 #    ###  #### ####  ##   ##  #### ####   
 #    #  #    # #    #  # #  # #    #      
 #    #  #   #  ###  #    #    ###  ###    
 #    ###   #   #    #    # ## #    #      
 #    # #  #    #    #  # #  # #    #      
 #### #  # #### ####  ##   ### #    ####   
```


## Final Output

To output parts 1 and 2: 
```bash
python d11.py -r 3 -p1 d11p1data.txt

Part 1:
HALT!
Min: (-19-47j) Max: (33+31j)
Already painted: 7351
Part 1 solution: 2160

Part 2:
HALT!
Min: 0j Max: (5+42j)
Already painted: 1
 #    ###  #### ####  ##   ##  #### ####   
 #    #  #    # #    #  # #  # #    #      
 #    #  #   #  ###  #    #    ###  ###    
 #    ###   #   #    #    # ## #    #      
 #    # #  #    #    #  # #  # #    #      
 #### #  # #### ####  ##   ### #    ####   
 ```