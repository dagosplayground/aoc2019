#!/usr/bin/python
"""
Python 3 for AoC 2019 Day 07
https://adventofcode.com/2019/day/7
"""

import argparse
from itertools import permutations 

def ParameterMode(opcode): 
    """Parses the output of parameter mode"""

class computer:

    def __init__(self,intArrayInput,savedPointer,ampInput):
        """Meat and potatoes of this challenge for parts 1 and 2. 
        Takes an integer array of 'operations and parameters' as defined in the 
        problem. Outputs the result in [0], or their lame excuse of an 
        accumlator. Regardless, WORST PROCESSOR EVER!!! Give me a case of beer
        and I'll write one better. I know, I did it as an intern.
        Cleaner description -------
        input: integer array
        output: only integer you care about.
        """
        self.stack = intArrayInput
        self.pointer = savedPointer
        self.ampInput = ampInput # Should be an int array of len 2
        self.ampInputCounter = 0

    def run(self):
        """Run through the program and push opcodes to functions"""
        try:
            while self.stack[self.pointer] != 99: 
                # print(f"Pointer: {self.pointer} ")
                instruction = self.stack[self.pointer] % 100 
                if len(str(self.stack[self.pointer])) > 2:
                    modes = str(self.stack[self.pointer])[:-2]
                else: 
                    modes = "0"
                if instruction == 1: # add
                    self.add(modes)
                elif instruction == 2: # multiply
                    self.multiply(modes) 
                elif instruction == 3: # Input 
                    self.program_input(modes) 
                elif instruction == 4: # output
                    return self.program_output(modes) 
                elif instruction == 5: # jump if true
                    self.jit(modes) 
                elif instruction == 6: # jump if zero
                    self.jiz(modes) 
                elif instruction == 7: # If less than
                    self.IfLessThan(modes)                    
                elif instruction == 8:
                    self.IfEqual(modes) # If equal
            else:
                print("HALT!")
                return "HALT"

            # else:
            #     print(f"Stack: { self.stack } ")        
        except:
            with open("error.txt","w") as errtxt: 
                print(f"fail on pointer {self.pointer} {self.stack[self.pointer]} ")
                errtxt.write(str(self.stack))
                raise 

    def getParams(modes,length):
        """Get the modes from a the inputs"""
        retmodes = [] 
        for index in modes[::-1]:
            retmodes.append(int(index))

        for i in range(length - len(retmodes)):
            retmodes.append(0)

        return retmodes

    def add(self, modes):
        """Addition function""" 
        params = computer.getParams(modes,3)


        arg1 = self.stack[self.stack[self.pointer+1]] if params[0] == 0 else self.stack[self.pointer+1]
        arg2 = self.stack[self.stack[self.pointer+2]] if params[1] == 0 else self.stack[self.pointer+2]

        if params[2]:
            self.stack[self.pointer+3] = arg1 + arg2
        else:
            self.stack[self.stack[self.pointer+3]] = arg1 + arg2
        self.pointer += 4         

    def multiply(self, modes):
        """Multiplication function""" 
        params = computer.getParams(modes,3)

        arg1 = self.stack[self.stack[self.pointer+1]] if params[0] == 0 else self.stack[self.pointer+1]
        arg2 = self.stack[self.stack[self.pointer+2]] if params[1] == 0 else self.stack[self.pointer+2]

        if params[2]:
            self.stack[self.pointer+3] = arg1 * arg2
        else:
            self.stack[self.stack[self.pointer+3]] = arg1 * arg2
        self.pointer += 4         

    def program_input(self,modes):
        """Takes input from a system when the insturction is given."""
        # modified for computer inputs 
        params = computer.getParams(modes,1)
        if self.ampInputCounter > 2:
            if params[0]:
                self.stack[self.pointer+1] = int(input(f"Set value at pointer {self.pointer+1}: "))
            else:
                self.stack[self.stack[self.pointer+1]] = int(input(f"Set value at pointer {self.pointer+1}: "))
        else:
            if params[0]:
                self.stack[self.pointer+1] = self.ampInput[self.ampInputCounter]
            else:
                self.stack[self.stack[self.pointer+1]] = self.ampInput[self.ampInputCounter]
            self.ampInputCounter += 1
        self.pointer += 2

    def program_output(self,modes):
        """Computer print out function"""
        params = computer.getParams(modes,1)
        outval = self.stack[self.pointer+1] if params[0] else self.stack[self.stack[self.pointer+1]]
        # print(f"Program output { outval }")
        self.pointer += 2
        return outval

    def jit(self,modes):
        """Jump if True
        If the first argument is not zero, set pointer to arg2"""
        params = computer.getParams(modes,2)
        arg1 = self.stack[self.stack[self.pointer+1]] if params[0] == 0 else self.stack[self.pointer+1]
        arg2 = self.stack[self.stack[self.pointer+2]] if params[1] == 0 else self.stack[self.pointer+2]

        if arg1:
            self.pointer = arg2
        else:
            self.pointer += 3


    def jiz(self,modes):
        """Jump if Zero
        If the first argument is zero, set pointer to arg2"""
        params = computer.getParams(modes,2)
        arg1 = self.stack[self.stack[self.pointer+1]] if params[0] == 0 else self.stack[self.pointer+1]
        arg2 = self.stack[self.stack[self.pointer+2]] if params[1] == 0 else self.stack[self.pointer+2]

        if not arg1:
            self.pointer = arg2
        else:
            self.pointer += 3

    def IfLessThan(self,modes):
        """Evaluate arg1 < arg2
        If true/false store 1/0 in the address of arg3"""
        params = computer.getParams(modes,3)
        arg1 = self.stack[self.stack[self.pointer+1]] if params[0] == 0 else self.stack[self.pointer+1]
        arg2 = self.stack[self.stack[self.pointer+2]] if params[1] == 0 else self.stack[self.pointer+2]
     
        if params[2]:
            self.stack[self.pointer + 3 ] = 1 if arg1 < arg2 else 0
        else:
            self.stack[self.stack[self.pointer + 3 ]] = 1 if arg1 < arg2 else 0

        self.pointer +=4

    def IfEqual(self,modes):
        """Evaluate arg1 == arg2
        If true/false store 1/0 in the address of arg3"""
        params = computer.getParams(modes,3)
        arg1 = self.stack[self.stack[self.pointer+1]] if params[0] == 0 else self.stack[self.pointer+1]
        arg2 = self.stack[self.stack[self.pointer+2]] if params[1] == 0 else self.stack[self.pointer+2]
        arg3 = self.stack[self.pointer+3] if params[2] == 0 else (self.pointer + 3)

        if params[2]:
            self.stack[self.pointer + 3 ] = 1 if arg1 == arg2 else 0
        else:
            self.stack[self.stack[self.pointer + 3 ]] = 1 if arg1 == arg2 else 0

        self.pointer +=4


def part1(p1FileIn,inphases=None):
    """Part1"""
    with open(p1FileIn,'r') as p1f:
        p1data = p1f.readlines()
        opcodes = [int(place) for place in p1data[0].split(',')] # computer instructions from text file. 
        phases = inphases.split(',')
        amp_out = 0
        if inphases != 'None':
            for ampPhase in phases: 
                intcomp = computer(opcodes,0,[int(ampPhase),amp_out])
                amp_out = intcomp.run()
        else:  
            params = permutations(range(5),5) # Sets the phases of the amps. 
            maxval = 0
            maxParams = None
            for ampPhase in params:
                amp_out = 0
                phaseString = str(ampPhase).strip('(').strip(')')
                for phase in phaseString.split(','):    
                    intcomp = computer(opcodes,0,[int(phase),amp_out])
                    amp_out = intcomp.run()
                if maxval < amp_out:
                    maxval = amp_out
                    maxParams = ampPhase

        print(f"Maxval: {maxval} Phases: {maxParams}")


#		print("\nPart 1 Answer:",str(computer(opcodes)))
	
def freshamps(ampPhases,original_opcodes):
    """Creates fresh amplifier data class between iterations"""
    amps = [None] * len(ampPhases)
    for amp in enumerate(ampPhases):
        amps[amp[0]] = [amp[1],original_opcodes,0,0]
        # Params are Phase, ampstack, amp_pointer, amp_out
    return amps

def AmplifierOuts(amps):
    """Debugging tool for showing output of amplifier"""
    ampouts = [] 
    for amp in amps:
        ampouts.append(amp[3])

    return ampouts

def part2(p2FileIn,inphases=None):
    """Part2"""
    with open(p2FileIn,'r') as p1f:
        p1data = p1f.readlines()
        opcodes = [int(place) for place in p1data[0].split(',')]
        phases = inphases.split(',')
        maxval = 0
        paramsIter = permutations(range(5,10),5) 
        single = False
        for ampPhases in paramsIter:
            amp_out = 0 
            amps = freshamps(ampPhases,opcodes)
            halted = False
            count = 0
            if inphases != 'None':
                if single: 
                    print("Break point should not be hit outside of testing")
                    break
                single = True
                ampPhases = [int(x) for x in inphases.strip('(').strip(')').split(',')]
                amps = freshamps(ampPhases,opcodes)
            while(not halted):
                # Put if statement  here to handle a specific phase sequence.
                print(f"Count { count } ampPhases: { ampPhases } amplifierOuts: { AmplifierOuts(amps) }  ")
                for amp in enumerate(amps):
                    intcomp = computer(amp[1][1], 0, [amp[1][0], amp_out]) if not count else computer(amp[1][1], amp[1][2], [amp_out])
                    compout = intcomp.run()
                    if compout == "HALT":
                        halted = True
                    else:  
                        amps[amp[0]][1] = intcomp.stack.copy() 
                        amps[amp[0]][2] = intcomp.pointer
                        # amps[amp[0]][1] = opcodes 
                        amp_out = compout
                        amps[amp[0]][3] = amp_out
                        with open("error.txt","a") as efile:
                            efile.write(f"Count { count } Phase: { amp[0] } ampPhases: { ampPhases } amplifierOuts: { AmplifierOuts(amps) }\n")
                            # efile.write(str(intcomp.stack[-6:-1])+"\n")
                            # efile.write(f"{ str(intcomp.stack) }\n")
                count += 1
                if count > 20:
                    print("program failed")
                    return

                if maxval < amps[4][3]:  # Thruster E signal. 
                    maxval = amps[4][3]
                    maxParams = ampPhases
            else:
                print(f"Amp E output: { amps[4][3] }")
        print(f"Max thruster signal: { maxval } ")
    #     print(f"Maxval: {maxval} Phases: {maxParams}")
    pass

def main(parts,fileNames,targetValue=0):
    """Main method to call both parts of the problem. """

    if parts == 3 or parts == 1:
        part1(fileNames[0],targetValue)   
    if parts == 3 or parts == 2:
        part2(fileNames[0],targetValue)

if __name__ == '__main__':
    # Accept command line arguments in a cleaner like fashion. 

    parser = argparse.ArgumentParser(description='Advent of Code 2019 Day 07: Invalid Instruction')
    parser.add_argument('-p1','--d02p1File', help="Name of the Day 07 part 1 input text file")
    parser.add_argument('-i1','--ampInputs', help="Phase Inputs of part 1") 
    parser.add_argument('-p2','--d02p2File', help="Name of the Day 07 part 2 input text file") 
    parser.add_argument('-r','--runpart', type=int, help="Set which part to run, 1 for part 1, 2 for part 2, 3 for part 1 and 2.")

    args = parser.parse_args()
    infiles = [args.d02p1File, args.d02p2File] # Lazy again, should only need the first name. 
    ampphases = str(args.ampInputs)
    main(args.runpart,infiles,ampphases)

